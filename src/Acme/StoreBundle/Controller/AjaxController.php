<?php

namespace Acme\StoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Acme\StoreBundle\Document\products;

class AjaxController extends Controller
{

    public $success;
    private $data;
    private $message;

    public function ajaxAction(Request $request)
    {

        $name = $request->query->get('name');
        $price = $request->query->get('price');
        $img = $request->query->get('img');

        $json = array($name,$price,$img);

        $this->validateRequest($json);


        if($this->success === true)
        {
            $json = array("success"=>true,"message"=>$this->message);
        }
        else
        {
            $json = array("success"=> false,"message"=>$this->message);

        }
        $response = new Response(json_encode($json));
        $response->headers->set('Content-Type', 'application/json');
        return $response;

    }

    private function insert($json){

        try
        {
            $product = new products();
            $product->setName($json[0]);
            $product->setPrice($json[1]);
            $product->setImg($json[2]);

            $dm = $this->get('doctrine_mongodb')->getManager();
            $dm->persist($product);
            $dm->flush();

//            $id = $product->getId();
            $this->message = "successfully saved";
            $this->success=true;
            $this->data=$json;

        }
        catch(\Doctrine\ORM\ORMException $e)
        {
            // flash msg
            $this->get('session')->getFlashBag()->add('error', 'couldtn save to db');
            $this->get('logger')->error($e->getMessage());

            $this->success = false;
            $this->data = $e;
            $this->message = "ORMException";

        }
        catch(\Exception $e)
        {
            $this->success = false;
            $this->message = "Normal Exception";

        }

    }

    private function validateRequest($json)
    {
        if(($json[0] != "" && $json[1] != "" && $json[2] != ""))
        {
            $this->insert($json);
        }
        else
        {
            $this->message = "item is missing";
        }
    }


}
