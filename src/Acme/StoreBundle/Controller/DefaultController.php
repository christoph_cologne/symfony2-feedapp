<?php

namespace Acme\StoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Acme\StoreBundle\Document\products;



class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('AcmeStoreBundle:Default:index.html.twig', array('name' => $name));
    }

    public function insertAction()
    {

        $inputs = array(

            array(
                "id"=>"productName",
                "name"=>"name",
                "type"=>true
            ),
            array(
                "id"=>"productPrice",
                "name"=>"price",
                "type"=>true
            ),
            array(
                "id"=>"productIMG",
                "name"=>"image url",
                "type"=>true
            ),
            array(
                "id"=>"productIMGLoad",
                "name"=>"product image",
                "type"=>false
            )

        );

        return $this->render('AcmeStoreBundle:Default:insert.html.twig',array("inputs"=>$inputs));



    }


    public function mongoAction()
    {
        $product = new products();
        $product->setName('A Foo Bar');
        $product->setPrice('19.99');

        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->persist($product);
        $dm->flush();


        $id = $product->getId();
        return $this->render('AcmeStoreBundle:Default:mongo.html.twig',array('state'=> $id));
    }




}
