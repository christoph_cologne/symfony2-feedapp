<?php

namespace Acme\StoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class JSONController extends Controller
{




    public function showAction()
    {

        $cursor = $this->dbcon();

        $string = "[";

        $c = 1;
        foreach ($cursor as $product) {
            $string.= "{\"name\":\"".$product->getName()."\",";
            $string.="\"price\":".$product->getPrice().",";
            $string.="\"img\":[";
            $p = 1;
            foreach($product->getImg() as $img){
                $string.= "\"".$img."\"";
                if($p != count($product->getImg())){
                    $string.= ",";
                }

                $p++;
            }
            $string .= "]";

            $string.="}";
            if($c != count($cursor)){
                $string .= ",";
            }
            $c++;

        }
//        $string = iterator_to_array($query->execute());
        $string .= "]";


        $response = new Response($string);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function testAction(){
        $cursor = $this->dbcon();
        foreach ($cursor as $product) {

            echo "<pre>";
            print_r($product->getImg());
            echo "</pre>";
        }

        $response = new Response("");
        return $response;

    }


    private function dbcon(){
        $dm = $this->get('doctrine_mongodb')->getManager();

        $qb = $dm->createQueryBuilder('AcmeStoreBundle:products')
            ->eagerCursor(true);



        $query = $qb->getQuery();
        $cursor = $query->execute();
        return $cursor;
    }

}
