<?php

namespace Acme\StoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class NotificationController extends Controller
{

    public function appleAction(Request $request)
    {

        $kernel = $this->get('kernel');

        $certPath = $kernel->locateResource('@AcmeStoreBundle/Resources/certs/ck.pem');
        $deviceToken = "ec32fdd67b585437ee2dd51c35e0fe6805ebe970089c4349a4d41881b364b703";


// Put your private key's passphrase here:
        $passphrase = '2015';

// Put your alert message here:
        $message = $request->request->get("message","");

////////////////////////////////////////////////////////////////////////////////

        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', $certPath);
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

// Open a connection to the APNS server
        $fp = stream_socket_client(
            'ssl://gateway.sandbox.push.apple.com:2195', $err,
            $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);

        echo 'Connected to APNS' . PHP_EOL;

// Create the payload body
        $body['aps'] = array(
            'alert' => $message,
            'sound' => 'default'
        );

// Encode the payload as JSON
        $payload = json_encode($body);

// Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

// Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));

        if (!$result)
            echo 'Message not delivered' . PHP_EOL;
        else
            echo 'Message successfully delivered' . PHP_EOL;

// Close the connection to the server
        fclose($fp);


        return $this->render('AcmeStoreBundle:Notification:push.html.twig',array());
    }


    public function androidAction(){

        return $this->render('AcmeStoreBundle:Notification:push.html.twig',array());
    }

    public function pushAction(){

        $forms =

                    array(

                     "inputs" =>  array(
                                        array(
                                            "name" => "Description",
                                            "type" => "text",
                                            "id"  => "decsr"
                                            )
                                        ),
                     "submitString" => "Submit"

                        );


        return $this->render('AcmeStoreBundle:Notification:push.html.twig',$forms);


    }



}
