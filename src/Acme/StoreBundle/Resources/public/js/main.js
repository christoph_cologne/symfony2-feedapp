    function nc(t,tp){
        var nc = $(".nc");
        nc.addClass("ncAct " + tp);
        $("#ncText").text(t);
        setTimeout(
            function(){
                nc.removeClass("ncAct "+ tp);
            }, 2000);
    }
    function ajax(url,type,data,dataType,success,error){
        $.ajax({
                url:url,
                type:type,
                data:data,
                dataType:dataType,
                success:function(json){
                    success(json)
                },
                error:function(e){
                    error(e)
                }
        });
    }

var URLS = {
            "INSERT":"http://localhost/symfony2/web/ajax",
             "PUSH":"http://localhost/symfony2/web/applepush/"
            }

$(document).ready(function(){
    function insertSuccess(json){
        if(json.success === true){
            nc(json.message,"sNC");
            $(".tInput").val("");
            $("#productName").focus();
        }
        else{
            nc(json.message,"eNC");
        }



    }
    function insertError(e){
        nc("ajax error","eNC");
    }

    $("#productName").focus();
    function sendRequest(url){
        var data = {name:$("#productName").val(),price:parseFloat($("#productPrice").val()),img:$("#productIMG").val()};
        ajax(url , "GET",data,"JSON",insertSuccess,insertError)
    }


    $(".go").on("click",function(){
        sendRequest(URLS.INSERT)
    });
    $("#submitPush").on("click",function(){
        ajax(URLS.PUSH,"POST",{"message":$("#decsr").val()},"JSON",success,insertError)
    });

    function success(json){
        alert("succes")
    }



    $(window).keydown(function (e){
        if ( e.which === 13  && e.metaKey ) {
            sendRequest(URLS.INSERT)
        }

    });




});